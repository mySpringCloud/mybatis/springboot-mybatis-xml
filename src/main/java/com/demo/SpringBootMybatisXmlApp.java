package com.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.demo.dao")
public class SpringBootMybatisXmlApp {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SpringBootMybatisXmlApp.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMybatisXmlApp.class, args);
	}

}
