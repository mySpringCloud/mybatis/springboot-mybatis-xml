package com.demo.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration 
//@MapperScan(basePackages = "com.demo.dao", sqlSessionFactoryRef = "sqlSessionFactory")
public class DataSourceConfig {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DataSourceConfig.class);

	@Bean(name = "dataSource")
	@Primary
//	@ConfigurationProperties(prefix = "spring.datasource.oracle")
	public DataSource testDataSource(@Autowired OracleConfig oracleConfig) {
//		return DataSourceBuilder.create().build();
		DruidDataSource dataSource = new DruidDataSource();
		
		dataSource.setConnectionProperties("config.decrypt=true;config.decrypt.key="+oracleConfig.getPublicKey());
		try {
			dataSource.setFilters("config,stat");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		dataSource.setUrl(oracleConfig.getUrl());
		dataSource.setUsername(oracleConfig.getUsername());
		dataSource.setPassword(oracleConfig.getPassword());
        dataSource.setDriverClassName(oracleConfig.getDriverClassName());
        dataSource.setMinIdle(oracleConfig.getMinIdle());
        dataSource.setMaxActive(oracleConfig.getMaxActive());
        dataSource.setInitialSize(oracleConfig.getInitialSize());
        dataSource.setMaxWait(oracleConfig.getMaxWait());
        dataSource.setTimeBetweenEvictionRunsMillis(oracleConfig.getTimeBetweenEvictionRunsMillis());
        dataSource.setMinEvictableIdleTimeMillis(oracleConfig.getMinEvictableIdleTimeMillis());
        dataSource.setValidationQuery(oracleConfig.getValidationQuery());
        dataSource.setTestWhileIdle(oracleConfig.isTestWhileIdle());
        dataSource.setTestOnBorrow(oracleConfig.isTestOnBorrow());
        dataSource.setTestOnReturn(oracleConfig.isTestOnReturn());
        dataSource.setPoolPreparedStatements(oracleConfig.isPoolPreparedStatements());
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(oracleConfig.getMaxPoolPreparedStatementPerConnectionSize());
		
		LOG.info("分布式事物dataSource实例化成功");
        return dataSource;
	}

//	@Bean(name = "sqlSessionFactory")
//	@Primary
//	public SqlSessionFactory testSqlSessionFactory(@Qualifier("dataSource") DataSource dataSource) throws Exception {
//		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//		bean.setDataSource(dataSource);
//
//		/*
//		 * bean.setMapperLocations( new
//		 * PathMatchingResourcePatternResolver().getResources(
//		 * "classpath:mybatis/mapper/test1/*.xml"));
//		 */
//		return bean.getObject();
//	}
//
//	@Bean(name = "transactionManager")
//	@Primary
//	public DataSourceTransactionManager testTransactionManager(@Qualifier("dataSource") DataSource dataSource) {
//		return new DataSourceTransactionManager(dataSource);
//	}
//
//	@Bean(name = "sqlSessionTemplate")
//	@Primary
//	public SqlSessionTemplate testSqlSessionTemplate(
//			@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
//		return new SqlSessionTemplate(sqlSessionFactory);
//	}

}
