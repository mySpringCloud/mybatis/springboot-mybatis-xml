package com.demo;

import com.alibaba.druid.filter.config.ConfigTools;

public class Druid密码加密解密 {

	public static void main(String[] args) throws Exception {
		//第一种加密方法
		//到druid-1.1.10.jar目录下打开cmd窗口，执行以下命令为密码ZHUwen12加密，随后获得公钥public key
		//java -cp druid-1.1.10.jar com.alibaba.druid.filter.config.ConfigTools ZHUwen12
		
		//第一种解密方法
		String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALESqBkd/4Kne+NVwVLirUBsXKTYlsOJjgNQ7bcwzbPn0c0Wv6h+KXPHbwkOJiuz6b4H+GBcB+HuELwiTrTB4dsCAwEAAQ==";
		String encryptPassword = "C3Kcc9g8m/xel7VgakJKXFJcCl7IiOc65r0o5hpi0qP4LjQ46s82T1UeRHfwzZfP0QC4GVpv7j3H5YfxckBnqg==";
		String decryptPassword = ConfigTools.decrypt(publicKey, encryptPassword);
		System.out.println("decryptPassword==="+decryptPassword);
		
		//第二种加密方法
		String pwd = "ZHUwen12";
		String encryptPwd = ConfigTools.encrypt(pwd);
		System.out.println("加密后："+encryptPwd);
		
		//第二种解密方法
		String decryptPwd = ConfigTools.decrypt(encryptPwd);
		System.out.println("解密后："+decryptPwd);

	}

}
